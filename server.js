'use strict';

var express = require('express');
var bodyParser = require('body-parser');
var logger = require('morgan');
var mnsig = require('mnsig-js-client');


function callback(res, err, json) {
  if (err) {
    res.status(err.statusCode).json({error: err.statusMessage});
    return;
  }
  res.json(json);
}

var app = express();
var client = new mnsig.Client();

app.use(logger('combined'));
app.use(bodyParser.json({limit: '8mb'}));

app.use(function(req, res, next) {
  var token = req.get('token');
  if (token) {
    client.token = token;
  }
  next();
});

app.post('/config', function(req, res) {
  client.instance = req.body.instance;
  var success = (!!client.instance);
  res.json({success: success});
});

app.post('/wallet/', function(req, res) {
  client.createWallet(req.body, function(err, json) {
    callback(res, err, json);
  });
});

app.put('/wallet/', function(req, res) {
  client.addWalletKey(req.body, function(err, json) {
    callback(res, err, json);
  });
});

app.get('/wallet/:wallet', function(req, res) {
  var wallet = req.params.wallet;
  client.walletInfo({wallet: wallet}, function(err, json) {
    callback(res, err, json);
  });
});

app.get('/wallet/:wallet/keys', function(req, res) {
  var wallet = req.params.wallet;
  client.walletKeys({wallet: wallet}, function(err, json) {
    callback(res, err, json);
  });
});

app.get('/wallet/:wallet/balance', function(req, res) {
  var params = {wallet: req.params.wallet};
  if (req.query.target !== undefined) {
    params.target = req.query.target;
  }
  client.balance(params, function(err, json) {
    callback(res, err, json);
  });
});

app.post('/wallet/:wallet/address', function(req, res) {
  req.body.wallet = req.params.wallet;
  client.newAddress(req.body, function(err, json) {
    callback(res, err, json);
  });
});

// Requires web token.
app.post('/wallet/:wallet/token/', function(req, res) {
  req.body.wallet = req.params.wallet;
  client.addToken(req.body, function(err, json) {
    callback(res, err, json);
  });
});

// Requires web token.
app.delete('/wallet/:wallet/token/:tokenId', function(req, res) {
  var params = {wallet: req.params.wallet, tokenId: req.params.tokenId};
  client.deleteToken(params, function(err, json) {
    callback(res, err, json);
  });
});

// Requires web token.
app.post('/wallet/:wallet/user', function(req, res) {
  req.body.wallet = req.params.wallet;
  client.addWalletUser(req.body, function(err, json) {
    callback(res, err, json);
  });
});

app.get('/wallet/:wallet/transactions', function(req, res) {
  var params = req.query;
  params.wallet = req.params.wallet;
  client.txlist(params, function(err, json) {
    callback(res, err, json);
  });
});

app.get('/wallet/:wallet/transaction', function(req, res) {
  var params = req.query;
  params.wallet = req.params.wallet;
  client.txget(params, function(err, json) {
    callback(res, err, json);
  });
});

app.get('/wallet/:wallet/utxos', function(req, res) {
  var params = {wallet: req.params.wallet};
  client.listUTXOs(params, function(err, json) {
    callback(res, err, json);
  });
});

app.get('/wallet/:wallet/estimatefee/:target', function(req, res) {
  client.estimateFee(req.params, function(err, json) {
    callback(res, err, json);
  });
});

app.post('/transaction/proposal/', function(req, res) {
  client.startProposal(req.body, function(err, json) {
    callback(res, err, json);
  });
});

app.put('/transaction/proposal/', function(req, res) {
  client.prepareProposal(req.body, function(err, json) {
    callback(res, err, json);
  });
});

app.post('/transaction/proposal/many', function(req, res) {
  client.startManyProposal(req.body, function(err, json) {
    callback(res, err, json);
  });
});

app.get('/transaction/:wallet/proposal/:id', function(req, res) {
  client.getProposal(req.params, function(err, json) {
    callback(res, err, json);
  });
});

app.delete('/transaction/:wallet/proposal/:id', function(req, res) {
  client.deleteProposal(req.params, function(err, json) {
    callback(res, err, json);
  });
});

app.post('/transaction/proposal/sign', function(req, res) {
  var proposal = req.body.proposal;
  var xpriv = req.body.xpriv;
  client.localSign(proposal, xpriv, function(err, json) {
    callback(res, err, json);
  });
});

app.post('/transaction/broadcast', function(req, res) {
  client.broadcast(req.body, function(err, json) {
    callback(res, err, json);
  });
});

app.get('/transaction/:wallet/:txid', function(req, res) {
  client.txinfo(req.params, function(err, json) {
    callback(res, err, json);
  });
});


var port = process.env.PORT || 7711;
var host = process.env.HOST || '127.0.0.1';
var listener = app.listen(port, host, function() {
  console.log('Listening on ' + JSON.stringify(listener.address()));
});
