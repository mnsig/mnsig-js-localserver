## Using mnsig-js-localserver

There are two recommended ways to use the API from https://gitlab.com/mnsig/mnsig-backend. One way is to use the JavaScript package mnsig-js-client, another one is to run the Express server from https://gitlab.com/mnsig/mnsig-js-localserver. The latter uses the former and is the recommended way in case you're not integrating mnsig into a system using nodejs.

Steps to setup mnsig-js-localserver:

    git clone https://gitlab.com/mnsig/mnsig-js-localserver.git
    cd mnsig-js-localserver
    npm install
    node server.js

This will start a expressjs server that accepts requests at http://127.0.0.1:7711. To listen to a different interface and/or port, specify the `HOST` and `PORT` env vars. There's only endpoint that is specific to mnsig-js-localserver: **/config**. The **/config** endpoint expects a POST JSON request with the `"instance"` parameter set to a string, the value for `"instance"` should be the name of your mnsig instance. Similar to using the mnsig API directly, every request to mnsig-js-localserver must send a `"Token"` header containing the API token to be used on that request.

Visit the [wiki](https://gitlab.com/mnsig/mnsig-js-localserver/wikis/home) for the complete documentation.
